angular.module('app.controllers', [])

    .controller('noticiasCtrl', function($scope, storiesService, $http,$cordovaSocialSharing, $ionicScrollDelegate, $timeout, $stateParams, urlService) {
        
        $scope.shareNoticias = function(){
            $cordovaSocialSharing.share('', 'Utilize agora mesmo o Notícias Online! Veja notícias do mundo inteiro a qualquer momento!', '', 'https://play.google.com/store/apps/details?id=br.com.orionx.noticiasonline');
        }

        $scope.hasMoreData = true;

        $scope.stories = [];

        function toTimestamp(strDate){
            var datum = Date.parse(strDate);
            return datum/1000;
        }

        function loadStories(params, callback){
            
            $http.get(urlService.url, {params: params})
                .success(function(response){

                    var stories = [];
                    angular.forEach(response.obj, function(child){
                        //stories.pubdate = toTimestamp(new Date());
                        if(child.meta_logo){
                            if(child.meta_logo.indexOf('http')>-1){
                                child.thumbnail = child.meta_logo;
                            }else
                                child.thumbnail = (child.thumbnail != '' ? child.thumbnail : 'img/logo/'+child.meta_logo);
                        }else{
                            child.thumbnail = 'img/noticias_online_logo.png';
                        }

                        stories.push(child);
                    });


                    callback(stories);
                });
        }

        $scope.loadOlderStories = function(){
            var params = {};
            if($scope.stories.length > 0){
                var params = {'lt': $scope.stories[$scope.stories.length -1].pubdate};
            }
            if($stateParams.type){
                params.category = $stateParams.type;
            }
            loadStories(params, function(olderStories){
                if(olderStories.length<1)
                    $scope.hasMoreData = false;

                else
                    $scope.stories = $scope.stories.concat(olderStories);

                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        $scope.loadNewerStories = function(){
            var params = {'gt': $scope.stories[0].pubdate};

            if($stateParams.type){
                params.category = $stateParams.type;
            }

            loadStories(params, function(newerStories){
                $scope.stories = newerStories.concat($scope.stories);
                //$scope.stories = $scope.stories;
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        function getTipoCategoria(type){
            if(type == 'POLITICA') return ' - Política';
            else if(type == 'GERAL') return ' - Geral';
            else if(type == 'FUTEBOL') return ' - Futebol';
            else if(type == 'FOFOCA') return ' - Fofocas';
            else if(type == 'TECNOLOGIA') return ' - Tecnologia';
            else if(type == 'ESPORTE') return ' - Esportes';
            else if(type == 'ECONOMIA') return ' - Economia';
        }

        $scope.tipoCategoria = getTipoCategoria($stateParams.type);
    })

    .controller('noticiaPertoDeMimCtrl', function($scope, $cordovaGeolocation, $http, $stateParams, urlService) {

        $scope.hasMoreData = true;
        $scope.stories = [];
        $scope.state = localStorage.getItem("state");
        $scope.city = localStorage.getItem("city");


        function hasNoLocation(){
            return (!localStorage.getItem("state") && !localStorage.getItem("city"));
        }

        function getLocation(){
            var posOptions = {timeout: 10000, enableHighAccuracy: false};
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    var lat  = position.coords.latitude
                    var long = position.coords.longitude

                    $http.get('http://nominatim.openstreetmap.org/reverse?format=json&lat='+lat+'&lon='+long+'&zoom=18&addressdetails=1').success(function(data) {
                        $scope.state = data.address.state;
                        if(data.address.city){
                            $scope.city = data.address.city;
                        }else{
                            $scope.city = data.address.town;
                        }

                        // configService.saveOrUpdateSubValorByChave(localStorage.getItem("city"), 'city', function() {});
                        // configService.saveOrUpdateSubValorByChave(localStorage.getItem("state"), 'state', function() {});

                        localStorage.setItem("state", $scope.state);
                        localStorage.setItem("city", $scope.city);

                        var params = {'category': localStorage.getItem("state")};
                        loadStories(params, function(locationStories){
                            $scope.stories = $scope.stories.concat(locationStories);

                            $scope.$broadcast('scroll.refreshComplete');
                        });
                    }).error(function(data) {

                    });

                }, function(err) {

                });
        }

        function loadStories(params, callback){
            params.category = localStorage.getItem("state");
            $http.get(urlService.url, {params: params})
                .success(function(response){
                    var stories = [];
                    angular.forEach(response.obj, function(child){
                        if(child.meta_logo){
                            if(child.meta_logo.indexOf('http')>-1){
                                child.thumbnail = child.meta_logo;
                            }else
                                child.thumbnail = (child.thumbnail != '' ? child.thumbnail : 'img/logo/'+child.meta_logo);
                        }else{
                            child.thumbnail = 'img/noticias_online_logo.png';
                        }

                        stories.push(child);
                    });
                    callback(stories);
                });
        }

        $scope.loadOlderStories = function(){


            if(hasNoLocation() || localStorage.getItem("city") === 'undefined'){
                getLocation();
                }else {

                var params = {};
                if ($scope.stories.length > 0) {
                    var params = {'lt': $scope.stories[$scope.stories.length - 1].pubdate};
                } else {
                    var params = {'lt': new Date()};
                }
                if ($stateParams.type) {
                    params.category = $stateParams.type;
                }
                loadStories(params, function (olderStories) {
                    if (olderStories.length < 1)
                        $scope.hasMoreData = false;

                    else
                        $scope.stories = $scope.stories.concat(olderStories);

                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
            }
        };

        $scope.loadNewerStories = function(){
            var params = {'gt': $scope.stories[0].pubdate};

            if($stateParams.type){
                params.category = $stateParams.type;
            }

            loadStories(params, function(newerStories){
                $scope.stories = newerStories.concat($scope.stories);
                $scope.$broadcast('scroll.refreshComplete');
            });
        };


    })

    .controller('configuracoesCtrl', function($scope, $ionicPopup, $timeout, $cordovaGeolocation, $http, $ionicLoading, configService) {

        // $scope.checkboxNotificacoes = {
        //     receber : true
        // };

        // function receberNotificacoes(valor){
        //     alert(valor);
        // }

        function hideLoading(){
            $ionicLoading.hide();
        }

        function getLocation(){
            var posOptions = {timeout: 10000, enableHighAccuracy: false};
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    var lat  = position.coords.latitude
                    var long = position.coords.longitude

                    $http.get('http://nominatim.openstreetmap.org/reverse?format=json&lat='+lat+'&lon='+long+'&zoom=18&addressdetails=1').success(function(data) {
                        localStorage.setItem("state", data.address.state);
                        if(data.address.city){
                            localStorage.setItem("city", data.address.city);
                        }else{
                            localStorage.setItem("city", data.address.town);
                        }

                        configService.saveOrUpdateSubValorByChave(localStorage.getItem("city"), 'city', function() {});
                        configService.saveOrUpdateSubValorByChave(localStorage.getItem("state"), 'state', function() {});

                        hideLoading();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Localização atualizada com sucesso!'
                        });


                    }).error(function(data) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Ups :(',
                            template: 'não conseguimos atualizar sua Localização agora, tente novamente daqui a pouquinho por favor!'
                        });
                    });

                }, function(err) {

                });
        }

        $scope.refresh_location = function(){
            $ionicLoading.show({
                template: 'Atualizando sua localização...'
            });
           getLocation();
        };
    })

    .controller('noticiaCompletaCtrl', function($scope,$window, $ionicLoading, $stateParams, storiesService, $sce, $cordovaSocialSharing) {
        $ionicLoading.show({
            template: 'Carregando :)'
        });

        params = {
            _id: $stateParams.type
        };
        storiesService.getStories(params, function(story) {

            $scope.story = story;

            $scope.content = $sce.trustAsHtml(story.content);

            //stories.pubdate = toTimestamp(new Date());
            if($scope.story.meta_logo){
                $scope.story.thumbnail = ($scope.story.thumbnail? $scope.story.thumbnail : 'img/logo/'+$scope.story.meta_logo);
            }else{
                $scope.story.thumbnail = 'img/noticias_online_logo.png';
            }


            $ionicLoading.hide();
        });

        $scope.shareAnywhere = function() {
            $cordovaSocialSharing.share('', $scope.story.title, '', $scope.story.link + ' Compartilhado via @Noticias Online');
        }

        $scope.readMore = function(){
            cordova.ThemeableBrowser.open($scope.story.link, '_blank', {
                statusbar: {
                    color: '#4f5d73ff'
                },
                toolbar: {
                    height: 44,
                    color: '#4f5d73ff'
                },
                title: {
                    color: '#ffffffff',
                    showPageTitle: true,
                    staticText: 'Notícias Online'
                },
                closeButton: {
                    wwwImage: '/img/arrow-back.png',
                    wwwImageDensity: 2,
                    align: 'left',
                    event: 'closePressed'
                },
                backButtonCanClose: true
            }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(e) {

            }).addEventListener(cordova.ThemeableBrowser.EVT_WRN, function(e) {

            });
        };

    })
