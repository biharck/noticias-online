angular.module('app.services', [])

.factory('storiesService', function($http){

    var dev = "http://localhost:3000/noticias-online-service/";
    var prd = "http://www.orionx.com.br/noticias-online-service/";

    // var url = dev;
    var url = prd;

    var getStories = function(params, callbackFn) {

        $http.get(url+params._id).success(function(data) {
            callbackFn(data.obj[0]);
        }).error(function(data) {
          console.log('erro');
        });
    };
    return {
        getStories: getStories
    };

})

.factory('urlService', function(){

    var dev = "http://localhost:3000/noticias-online-service";
    var prd = "http://www.orionx.com.br/noticias-online-service";

    // var url = dev;
    var url = prd;
    
    return {
        url : url
    };
})

.service('sqlService', [function(){

    var myDB = window.sqlitePlugin.openDatabase({name: "NoticiasOnline.db", location: 'default'});


    var selectByChave = function(callbackByChave){
        myDB.transaction(function(transaction) {
            transaction.executeSql('SELECT * FROM noticias_config', [], function (tx, results) {
                callbackByChave(results.rows);
            }, null);
        });
    };
    return {
        selectByChave: selectByChave
    };
}])


.service('configService', [function(sub_valor, chave){

    var myDB = window.sqlitePlugin.openDatabase({name: "NoticiasOnline.db", location: 'default'});

    var saveOrUpdateSubValorByChave = function(){
        myDB.transaction(function(transaction) {
            transaction.executeSql("SELECT * FROM noticias_config WHERE chave = ? ", [chave], function (tx, results) {

                if (results && results.rows.length > 0){
                    myDB.transaction(function (transaction) {
                        var executeQuery = "UPDATE noticias_config SET sub_valor=? WHERE chave=?";
                        transaction.executeSql(executeQuery, [sub_valor, chave],
                            //On Success
                            function (tx, result) {},
                            //On Error
                            function (error) {});
                    });

                }else {
                    myDB.transaction(function(transaction) {
                        var executeQuery = "INSERT INTO noticias_config (chave, sub_valor) VALUES (?,?)";
                        transaction.executeSql(executeQuery, [chave,sub_valor]
                            , function(tx, result) {},
                            function(error){});
                    });
                }
            }, null);
        });
    };
    return{
        saveOrUpdateSubValorByChave: saveOrUpdateSubValorByChave
    };

}])


;



