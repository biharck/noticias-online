angular.module('app.routes', [])

    .config(function($stateProvider, $urlRouterProvider) {

      $stateProvider
          .state('tabsController.noticias', {
            url: '/news',
            cache: true,
            views: {
              'tab1': {
                templateUrl: 'templates/noticias.html',
                controller: 'noticiasCtrl'
              }
            }
          })

          .state('tabsController.noticias_especificas', {
            url: '/news/:type',
            cache: true,
            views: {
              'tab1': {
                templateUrl: 'templates/noticias.html',
                controller: 'noticiasCtrl'
              }
            }
          })

          .state('tabsController.noticiaPertoDeMim', {
            url: '/news-nearby/:type',
            cache: true,
            views: {
              'tab2': {
                templateUrl: 'templates/noticiasPertoDeMim.html',
                controller: 'noticiaPertoDeMimCtrl'
              }
            }
          })

          .state('tabsController.configuracoes', {
            url: '/profile',
            views: {
              'tab3': {
                templateUrl: 'templates/configuracoes.html',
                controller: 'configuracoesCtrl'
              }
            }
          })

          .state('tabsController', {
            url: '/tabs-page',
            templateUrl: 'templates/tabsController.html',
            abstract:true
          })

          .state('tabsController.noticiaCompleta', {
            url: '/full-news/:type',
            cache: true,
            views: {
              'tab1': {
                templateUrl: 'templates/noticiaCompleta.html',
                controller: 'noticiaCompletaCtrl'
              }
            }
          })

      $urlRouterProvider.otherwise('/tabs-page/news')



    });