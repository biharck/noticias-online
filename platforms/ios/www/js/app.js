(function(){

var app = angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.services', 'app.directives', 'ngCordova', 'angularMoment'])


    app.run(function($ionicPlatform, $ionicPopup, $http, $timeout) {


        function rateNoticias(myDB){
            $ionicPopup.confirm({
                title: 'Notícias Online',
                template: 'Você gostou do Notícias Online? Avalie o aplicativo. Sua opinião é muito importante para nós!',
                buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
                    text: 'Ok :)',
                    type: 'button-positive',
                    onTap: function(e) {
                        var chave = 'QTD_OPENED';
                        var valor= 20;
                        myDB.transaction(function (transaction) {
                            var executeQuery = "UPDATE noticias_config SET valor=? WHERE chave=?";
                            transaction.executeSql(executeQuery, [valor, chave],
                                //On Success
                                function (tx, result) {
                                    localStorage.setItem("QTD_OPENED", valor);
                                    window.open('market://details?id=br.com.orionx.noticiasonline', '_system');
                                },
                                //On Error
                                function (error) {
                                });
                        });

                    }
                }, {
                    text: 'Agora não',
                    type: 'button-default',
                    onTap: function(e) {
                        //Igora e não faz nada
                    }
                }, {
                    text: 'Não',
                    type: 'button-default',
                    onTap: function(e) {
                        var chave = 'QTD_OPENED';
                        var valor= 30;
                        myDB.transaction(function (transaction) {
                            var executeQuery = "UPDATE noticias_config SET valor=? WHERE chave=?";
                            transaction.executeSql(executeQuery, [valor, chave],
                                //On Success
                                function (tx, result) {
                                    localStorage.setItem("QTD_OPENED", valor);
                                },
                                //On Error
                                function (error) {
                                });
                        });
                    }
                }]
            });
        }

        function showBanner(){
            AdMob.showBanner(AdMob.AD_POSITION.BOTTOM_CENTER);

        }

        function showBannerInterstitial(){
            AdMob.showInterstitial();
        }

        $ionicPlatform.ready(function() {

            if( ionic.Platform.isAndroid() )  {
                admobid = { // for Android
                    banner: 'ca-app-pub-6000628416104836/5778165902',
                    interstitial: 'ca-app-pub-6000628416104836/7168037105'
                };

                if(AdMob){
                    AdMob.createBanner( {
                        adId:admobid.banner,
                        position:AdMob.AD_POSITION.BOTTOM_CENTER,
                        autoShow:false
                    } );

                    AdMob.prepareInterstitial( {
                        adId:admobid.interstitial,
                        autoShow:false
                    });
                }

            }

            if(typeof analytics !== undefined) {
                analytics.startTrackerWithId("UA-82347035-1");
                analytics.trackView("noticiasCtrl");
            } else {
                console.log("Google Analytics Unavailable");
            }

            $timeout(showBanner, 10000);

            $timeout(showBannerInterstitial, 120000);

            //PRD mobile
            var myDB = window.sqlitePlugin.openDatabase({name: "NoticiasOnline.db", location: 'default'});
            // myDB.executeSql('DROP TABLE IF EXISTS noticias_config');
            myDB.executeSql('CREATE TABLE IF NOT EXISTS noticias_config (id integer primary key, chave text, valor integer, sub_valor text)');

            var push = PushNotification.init({ "android": {"senderID": "149682728592", icon : "icon"}});

            push.on('registration', function(data) {

                if(typeof analytics !== 'undefined') {
                    analytics.setUserId(data.registrationId);
                }

                $http.post('http://www.orionx.com.br/noticias-online-service/device_register',{token:data.registrationId, platform: 'ANDROID'}).then(function (res){
                    // alert('registrado');
                });
            });

            push.on('notification', function(data) {
                // alert('Notificação acionada, agora deve-se implementar a navegação no app de acordo com os dados: ' + JSON.stringify(data));
            });

            push.on('error', function(e) {
                // alert('registration error: ' + e.message);
            });


            myDB.transaction(function(transaction) {
                transaction.executeSql("SELECT * FROM noticias_config WHERE chave = 'QTD_OPENED' ", [], function (tx, results) {

                    // se já abriu alguma vez o aplicativo
                    if (results && results.rows.length > 0){
                        var valor = results.rows.item(0).valor;

                        valor = valor + 1;
                        localStorage.setItem("QTD_OPENED", valor);

                        if(valor <10){
                            var chave = 'QTD_OPENED';
                            // var valor= 2;
                            myDB.transaction(function (transaction) {
                                var executeQuery = "UPDATE noticias_config SET valor=? WHERE chave=?";
                                transaction.executeSql(executeQuery, [valor, chave],
                                    //On Success
                                    function (tx, result) {
                                    },
                                    //On Error
                                    function (error) {
                                    });
                            });
                        }

                    }else {
                        var chave="QTD_OPENED";
                        var valor = 1;
                        localStorage.setItem("QTD_OPENED", valor);
                        myDB.transaction(function(transaction) {
                            var executeQuery = "INSERT INTO noticias_config (chave, valor) VALUES (?,?)";
                            transaction.executeSql(executeQuery, [chave,valor]
                                , function(tx, result) {},
                                function(error){});
                        });

                    }
                }, null);
            });

            myDB.transaction(function(transaction) {
                transaction.executeSql("SELECT * FROM noticias_config WHERE chave = 'state' or chave = 'city' ", [], function (tx, results) {

                    // se já abriu alguma vez o aplicativo
                    if (results && results.rows.length > 0){
                        var len = results.rows.length, i;
                        for (i = 0; i < len; i++){
                            alert(results.rows.item(i))
                            localStorage.setItem(results.rows.item(i).chave, results.rows.item(i).sub_valor);
                        }
                    }
                }, null);
            });

            if(localStorage.getItem("QTD_OPENED") && (localStorage.getItem("QTD_OPENED") > 2 && localStorage.getItem("QTD_OPENED") <= 10)){
                rateNoticias(myDB);
            }

            if(window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if(window.StatusBar) {
                StatusBar.styleDefault();
            }


        });
    })

}());
